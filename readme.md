# tlip Class
### Trend log Import Processor

## Purpose
[ETL (Extract, transform, and load)](https://en.wikipedia.org/wiki/Extract,_transform,_load) of [building automation system (BAS)](https://en.wikipedia.org/wiki/Building_automation) trend log files.

Refactored from *opGraph* class (opGraph.py) original project for *City College Building Operators, 2013 Senior Design Project*.

## Version Info
Version: 0.1

Date: 04/29/2014

Author: James Hohman


## Requirements
 * Python 2.7.3
 * xlrd 0.9.2
 * numpy 1.8.0


## License, Copyright
Copyright James Hohman, 2015

All rights reserved. For example/educational/illustrative use only. Redistribution, republishing, and usage prohibited.