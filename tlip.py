'''  Name: tlip.py tlip class
           Trend log Import Processor
  Purpose: Import trend log files and post process them
           for AnyBAS application.
           Based on opGraph.py original project for
           City College Building Operators
           2013 Senior Design Project
  Version: 0.1
     Date: 04/29/2014
   Author: James Hohman

    Usage: Will complete later.

 Requires: xlrd, numpy
   Python: 2.7.3
'''

import xlrd  # @UnresolvedImport
import datetime
import sys
import os
import numpy as np
import string


class tlip():
    '''tlip class
    ### These Steps must be performed to prep the data

    We create by specifying the desired path and file (csv).
    tlip(fPath, fNameSource, Comments)    Initializes an empty tlip object.
    tlip.create()                         Read the CSV, create the dataset
    tlip.identifydelimiter()              Performs best guess of delimiter
    tlip.parse(delimiter)                 Parse the dataset
    tlip.row2col()                        Transpose the dataset rows-to-columns
                                          (Organizes the dataset logically)
    tlip.colmap()                         Generate the colmapDict (column
                                          mapping dictionary) for the dataset

    # tlip.getModelMeta()                 Perform the above steps
                                          automatically.

    fPath               = os.getcwd()   # Path to the CSV import/export
    Comments            = ['%%', '##']  # Comment markers within the CSV (even
                                          if they don't exist)
    delimiter           = '\t'          # The CSV field delimiter. In this case
                                          CSV is a misnomer, the data file is
                                          tab delimited.
    '''
    def __init__(self, fPath, fName, comments=None):
        self.__fPath__ = fPath
        self.__fName__ = fName
        self.comments = comments or []
        self.dataset = []
        self.cdate = datetime.datetime.now()  # time structure
        self.create_done = False
        self.parse_done = False
        self.identifydelimiter_done = False
        self.row2col_done = False
        self.colmap_done = False
        self.meta = {}
        self.modelMeta = {}
        self.delimiterSymbol = {'tabs': '\t',
                                'spaces': ' ',
                                'commas': ','
                                }
        self.formatTags = {'tabs': 'tsv',
                           'spaces': 'flat text',
                           'commas': 'csv',
                           'none': 'single column'
                           }

    def __iter__(self):
        for index in range(len(self.dataset)):
            yield self.dataset[index]

    def __len__(self):
        return len(self.dataset)

    def __str__(self):
        if len(self.dataset) == 0:
            return 'Empty dataset'
        else:
            tmpDataset = ''
            for column in self.dataset:
                tmpDataset += column
            return tmpDataset

    def __repr__(self):
        return '%s: %s' % (
            'tlip class', os.path.join(self.__fPath__, self.__fName__)
            )

    def __pp__(
            self, theCount,
            theStr='{0}', nthIter=1, incr=1, lastFlag=False
            ):
        '''
        Pretty Print Method, prints a counter and returns a count value.
        tlip.__pp__( Start Count, Format String, Print Iteration, Increment,
        Last Line Flag)
        Format String {0} where the count goes.
        Print every nth iteration.
        Step to increment the counter by.
        Print a Carriage Return (True) or not (False)
        '''
        theCountOld = theCount
        theCount += incr
        theStr = theStr.format(theCount)

        if lastFlag is True:  # Attach Carriage Return to Output
            theStrOld = theStr.format(theCountOld)
            theStrLenOld = len(theStrOld)

            print '\b' * (theStrLenOld + 2) + theStr
        else:                 # Suppress Carriage Return from Output
            if theCount % nthIter == 0:
                if incr >= 0:
                    theStrOld = theStr.format(theCount - (theCount % nthIter))
                else:
                    theStrOld = theStr.format(theCount + (theCount % nthIter))

                theStrLenOld = len(theStrOld)
                print '\b' * (theStrLenOld + 2) + theStr,
        return theCount

    def status(self):
        '''
        Prints the current status of the tlip object. This is basically an
        in-order checklist of the steps to properly instantiate an tlip object.
        '''
        print (
            '%20s: %s\n%20s: %s\n%20s: %s\n%20s: %s\n%20s: %s\n%20s: %s\n'
            ) % (
            'tlip class', os.path.join(self.__fPath__, self.__fName__),
            'Create', self.create_done,
            'Identify Delimiter', self.identifydelimiter_done,
            'Parse', self.parse_done,
            'Row to Column Map', self.row2col_done,
            'Column Map Dict', self.colmap_done,
            )

    def create(self):
        '''Reads the CSV data from disk and populates self.dataset.'''
        self.dataset = []
        try:
            theFile = open(os.path.join(self.__fPath__, self.__fName__), 'r')
            count = 0
            statusStr = '{0}: {1} lines read.'.format(self.__fName__, '{0}')
            # Display zeroth Count
            count = self.__pp__(count, statusStr, incr=0)
            for line in theFile:
                self.dataset.append(line.strip('\n'))
                # Display every 1000th count
                count = self.__pp__(count, statusStr, 1000)
            # Display Last Count
            count = self.__pp__(count, statusStr, lastFlag=True, incr=0)
            theFile.close()
            self.create_done = True
            self.records = count
        except:
            print 'Exception Occurred %s' % sys.exc_info()[1]
            self.create_done = False

    def colmap(self):
        '''
        Generate tlip.colmapDict (Column mapping dictionary) of dataset columns
        key: field name, value: column number
        '''
        self.colmapDict = {}
        numCols = len(self.dataset)
        for i in range(0, numCols):
            self.colmapDict[self.dataset[i][0]] = i
        self.colmap_done = True

    def parse(self, delimiter):
        '''
        Parse the input file by delimiter and add it to a self.dataset
        Need to strip any control characters. E.g. \n, \r, \r\n
        '''
        theList = []
        allBytes = string.maketrans('', '')  # String of 256 characters
        for row in self.dataset:
            tmpList = []
            for item in row.split(delimiter):
                # All bytes < 32 stripped.
                tmpList.append(item.translate(allBytes, allBytes[:32]))
            theList.append(tmpList)

        self.dataset = theList[:]
        self.parse_done = True
        self.headers = self.dataset[0][:]

    def row2col(self):
        '''
        Transpose the tlip.dataset from a list of rows to a list of columns.
        '''
        theList = []
        for j in range(0, len(self.dataset[0])):  # @UnusedVariable
            tmpCol = []
            for rowList in self.dataset:
                tmpCol.append(rowList.pop(0))
            theList.append(tmpCol)
        self.dataset = theList[:]
        self.row2col_done = True

    def performDTime(self):
        '''
        Creates a column of dt times in seconds
        Precondition: 'py_datetime' column exists, eg.,
            self.py_datetime(<timeformat>) was executed.
        Postcondition: 'dt' column appended to self.dataset in seconds,
            self.colmap() column mappings updated
        '''
        tCol = self.colmapDict['py_datetime']
        newCol = []

        for i in range(0, len(self.dataset[tCol])):
            if i == 0:
                newCol.append('dt')
            elif i == 1:
                newCol.append('0')
            else:
                newCol.append(str((
                    self.dataset[tCol][i] - self.dataset[tCol][i - 1]
                    ).total_seconds())
                )

        self.dataset.append(newCol)
        self.colmap()

    def py_datetime(self, timeFormat='datetime'):
        '''
        Copies the entire time column into python datetime objects
        as specified by <timeFormat> parameter (ex: 'datetime', 'date', 'time')
        into self.dataset 'py_datetime' column.
        '''
        tCol = self.colmapDict.get('time') or self.colmapDict.get('Time')
        newCol = []
        # If it's excel format use the excel datetime parser
        if self.modelMeta.get('excel'):
            for i in range(0, len(self.dataset[tCol])):
                if i == 0:
                    newCol.append('py_datetime')
                else:
                    newCol.append(datetime.datetime(*xlrd.xldate_as_tuple(
                        float(self.dataset[tCol][i]), 0))
                    )
        else:
            for i in range(0, len(self.dataset[tCol])):
                if i == 0:
                    newCol.append('py_datetime')
                else:
                    newCol.append(self.parsePyDatetime(
                        self.dataset[tCol][i], timeFormat)
                    )

        self.dataset.append(newCol)
        self.colmap()

    def parsePyDatetime(self, timeString, timeFormat='datetime'):
        '''
        Parses a time string into a python datetime object as specified
        by the <timeFormat> parameter (ex: 'datetime', 'date', 'time').
        Inputs:  <timeString>, eg: '2014/04/29 02:45:00 AM'
                 <timeFormat>, eg: 'datetime'
        Outputs: python datetime object of <timeFormat>
        '''
        if timeFormat == 'datetime':
            tcList = timeString.split(" ")
            date_part = tcList[0]
            time_part = tcList[1]
            pm_count = tcList[2].lower().count('pm')

            time_parts = time_part.split(":")
            hour = int(time_parts[0])
            if hour < 12:
                hour = hour + (pm_count * 12)
            else:
                hour = pm_count * 12

            minute = int(time_parts[1])
            try:
                sec = int(time_parts[2])
            except:
                sec = 0

            date_parts = date_part.split("/")
            month = int(date_parts[0])
            day = int(date_parts[1])
            if len(date_parts[2]) == 2:
                year = int("20" + date_parts[2])
            else:
                year = int(date_parts[2])

        return datetime.datetime(year, month, day, hour, minute, sec)

    def queryDataObject(self):
        '''Basic cmdline interface to query the data object.'''
        def presentColumns():
            """Present the list of available columns
            and return the list of valid selections"""
            validSelections = self.colmapDict.values()
            validSelections.sort()
            for selection in validSelections:
                for kv in self.colmapDict.iteritems():
                    if selection == kv[1]:
                        print '%2d - %s' % (kv[1], kv[0])

        uInput = 'a'
        while uInput != 'x':
            print
            print '=' * 80
            print 'Main Menu\n'
            validSelections = presentColumns()
            uInput = raw_input('\nSelect column to print, (a) all, (x) exit: ')
            try:
                uInput = int(uInput)
                if uInput in self.colmapDict.values():
                    print self.dataset[uInput]
                else:
                    print 'Not a valid selection.'
                    print 'Valid selections are: %s' % validSelections
            except:
                if uInput.lower() == 'x':
                    print 'Exiting...'
                elif uInput.lower() == 'a':
                    print self.dataset
                else:
                    print 'Not a valid selection.'
                    print 'Valid selections are: %s' % validSelections

        return validSelections

    def stats(self):
        '''
        Prints some basic stats regarding the tlip object and the dataset.
        '''
        print '%12s: %s%s' % ('Source', self.__fPath__, self.__fName__)
        print '%12s: %d' % ('Columns', len(self.dataset))
        for i in self.dataset:
            print '\n%12s: %s\n%12s: %d' % (
                'Column Name', i[0], 'Rows', len(i)
                )
            if '' in i:
                print '%12s: Empty fields detected.' % ('Warning')
            for row in i:
                if type(row) != str:
                    print '%12s: Non-string values detected. Type is %s' % (
                        'Warning', type(row)
                        )
                    break

    def identifydelimiter(self):
        '''
        Parses the first 1000 rows for meta analysis--attempts to identify the
        file delimiter intelligently.
        Output: self.delimiter = delimiter character
                self.identifydelimiter = True
        '''
        if self.create_done is False:
            raise Exception(
                'You must run .create() before .identifydelimiter() to '
                'initialize the dataset.'
                )
        else:
            '''
            Just parse the first 1000 rows for meta analysis, no need to do
            the whole thing
            '''
            self.meta['rows'] = len(self.dataset[:1000])
            for line in (self.dataset):
                self.meta['tabs'] = self.meta.get('tabs', 0) + line.count('\t')
                self.meta['commas'] = self.meta.get('commas', 0) + \
                    line.count(',')
                '''
                Removing space delimited file, as this complicates things.
                What if the file contains sentences?
                self.meta['spaces'] = self.meta.get('spaces', 0) +
                    line.count(' ')'''

            delimiter = max(
                self.meta.iterkeys(), key=(lambda key: self.meta[key])
                )

            if delimiter == 'rows':
                delimiter = 'none'

            self.delimiter = delimiter
            self.identifydelimiter_done = True

    def getModelMeta(self):
        '''
        Performs metadata analysis of the trend log file. Performs
        self.create(), self.identifydelimiter(), self.parse(<delimiter>),
        self.row2col(), self.colmap(). It performs these in order to extract
        trend log metadata.
        Precondition: tlip(fPath, fName, comments)
        Postcondition: data parsed, 'dt', 'py_datetime' columns appended,
                       self.modelMeta, self.dataset'''
        self.create()
        self.identifydelimiter()
        self.parse(self.delimiterSymbol[self.delimiter])
        try:
            self.row2col()
        except Exception as e:
            raise Exception('tlip.getModelMeta:row2col> %s' % (e))

        self.colmap()

        theFullPath = os.path.join(self.__fPath__, self.__fName__)

        mmDict = {'samples': self.records - 1,  # we assume it has headers
                  'sensors': len(self.dataset),
                  'formatTags': self.formatTags[self.delimiter],
                  'fileSize': os.path.getsize(theFullPath),
                  'date_file': datetime.datetime.fromtimestamp(
                      os.path.getmtime(theFullPath)
                      ),
                  'pathFilename': theFullPath,
                  'timeSeries': False,
                  'excel': False,
                  'date_startRange': None,
                  'date_endRange': None,
                  'avgInterval': None,
                  'headers': self.headers,
                  }

        # Check if its time series data
        if max(map(
                lambda key:  key.lower() == 'time', self.colmapDict.viewkeys())
               ):
            mmDict['timeSeries'] = True
            '''
            if we found a time column then it remove that column from the
            sensor count
            '''
            mmDict['sensors'] = len(self.dataset) - 1
            # if the minimum date turns out to be before 2000
            minimum_date = xlrd.xldate.xldate_from_date_tuple((2000, 1, 1), 0)
            '''
            then why are we processing them? So we assume time series data
            larger than this must be excel format.
            '''
            # if it is get the time column
            time_col = self.dataset[
                self.colmapDict.get('time') or self.colmapDict.get('Time')
                ]  # pulls the time column

            # verify the time column
            # pop the header
            if time_col[0].lower() == 'time':
                '''
                Check the minimum value. The logic here is, if it's
                time series based on seconds or minutes, then it starts low
                near zero.
                if it's excel it starts high, around the minimum_date

                let's look at the first 1000 entries (I think it's safe to
                assume that we'll find at least 1 time stamp in that range,
                or else something is really whacky
                IS IT EXCEL? First iteration, search for excel match'''
                excelError = ""
                is_12hour = False
                is_date = False
                is_time = False
                is_datetime = False

                i = 0
                for tc in time_col[1:min(1000, len(time_col))]:
                    i += 1
                    print "iter: ", i
                    # attempt to parse row as excel
                    try:
                        if float(tc) > minimum_date:
                            mmDict['excel'] = True
                        else:
                            excelError = (
                                'tlip.getModelMeta> Excel date evaluated to '
                                'less than 1-1-2000. Skipping file.'
                                )
                        break
                    except:
                        '''
                        Do nothing here. We get here when float(tc) excepts,
                        and we want to process all 1000 rows.
                        '''
                        pass

                    if excelError:
                        raise Exception(excelError)

                    '''
                    if it wasn't excel
                    IS IT TIMEFORMAT?
                    Indicates 12 hour format
                    '''
                    is_12hour = bool(max(
                        tc.lower().count('am'), tc.lower().count('pm'))
                        )

                    if tc.count("/") == 2:
                        is_date = True

                    if tc.count(":") >= 1:
                        is_time = True

                    if is_date and is_time:
                        is_datetime = True

                    if is_date or is_time or is_12hour:
                        break

                if mmDict['excel']:
                    mmDict['timeFormat'] = 'datetime'
                    '''
                    We must do this so that py_datetime can process the
                    times appropriately
                    '''
                    self.modelMeta = mmDict.copy()
                    stDate = datetime.datetime(
                        *xlrd.xldate_as_tuple(float(min(time_col[1:])), 0)
                        )
                    enDate = datetime.datetime(
                        *xlrd.xldate_as_tuple(float(max(time_col[1:])), 0)
                        )
                else:
                    if is_datetime and is_12hour:
                        mmDict['timeFormat'] = 'datetime'
                        timeFormat = 'datetime'
                        stDate = self.parsePyDatetime(time_col[1], timeFormat)
                        enDate = self.parsePyDatetime(time_col[-1], timeFormat)

                diff = enDate - stDate
                avgInterval = (diff/len(time_col[1:])).total_seconds()

                mmDict['date_startRange'] = stDate
                mmDict['date_endRange'] = enDate
                mmDict['avgInterval'] = avgInterval
            else:
                raise Exception(
                    'tlip.getModelMeta> Could not locate time column, '
                    'aborting.'
                    )

        '''
        Copy time column (excel or string) to python datetime format
        as specified in mmDict['timeformat'], eg, 'datetime'
        '''
        self.py_datetime(mmDict['timeFormat'])
        # Update column mappings
        self.colmap()
        # Perform DT times
        self.performDTime()
        self.modelMeta = mmDict.copy()

        return mmDict

    def cleanTimeValue(self, dataColIndex):
        '''This method will zip time-column with a data column. It then
        returns a list of lists, 0 - time, 1 - cleaned data (empty fields
        removed). This is to address the issue of a file that logs several
        sensors at different times. Thus the interval is inconsistent
        between sensors. However, for a particular sensor the interval
        is (hopefully) static.
        Precondition: <dataColIndex> the index of the data column to clean
        Postcondition: Tuple returned (List(py_datetime), List(values),
                       List(dt)) Cleaned of empty values.
        '''
        py_datetimeCol = self.colmapDict.get('py_datetime', None)
        col0 = []  # python datetimes
        col1 = []  # values
        col2 = []  # dt times

        if py_datetimeCol:
            i = 0
            for py, val in zip(
                    self.dataset[py_datetimeCol][:],
                    self.dataset[dataColIndex][:]
                    ):
                val = val.strip()
                if val:
                    i += 1
                    if i == 1:
                        col0.append(py)
                        col1.append(val)
                        col2.append('dt')
                    elif i == 2:
                        col0.append(py)
                        col1.append(val)
                        col2.append((py-py).total_seconds())
                    else:
                        '''
                        We changed the order here because it's more intuitive
                        that we get the last value in the list, rather than
                        append then grab the second to last value in the list.
                        print "py, col0[-1], dt: %s, %s, %s" % (py, col0[-1],
                        (py - col0[-1]).total_seconds())
                        '''
                        col2.append((py - col0[-1]).total_seconds())
                        col0.append(py)
                        col1.append(val)

            return col0[:], col1[:], col2[:]


def get_cleaned_column_meta(py_datetimeList, dtList):
    '''
    Function: Performs analytics on a cleaned column via self.cleanTimeValue or
    self.cleanTimeValueMinute. Takes a input list of python datetimes and does
    meta analysis.
    Precondition: cleanTimeValue(<value>) returned a valid tuple for input
    Postcondition: metaDict returned'''

    timeseries = False
    samples = len(py_datetimeList[1:])

    stDate = min(py_datetimeList[1:])
    enDate = max(py_datetimeList[1:])

    diff = enDate - stDate
    avgInterval = (diff/samples).total_seconds()

    # Arbitrary time series resolution
    if avgInterval % 60 >= 58 or avgInterval % 60 <= 2:
        timeseries = True

    return {'Samples': samples,
            'avgInterval': avgInterval,
            'timeseries': timeseries,
            'spread': set(dtList[2:]),
            'std_dev': np.std(dtList[2:])  # @UndefinedVariable
            }
